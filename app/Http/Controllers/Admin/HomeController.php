<?php

namespace App\Http\Controllers\Admin;
use App\Employee;
use App\Client;
class HomeController
{
    public function index()
    {
        $students = Client::count();
        $lecturer = Employee::count();

        return view('home', compact('students','lecturer'));
    }

}
