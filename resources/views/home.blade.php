@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            Home
        </div>
        <div class ="card container" style="width:100%">
        	<h5 class="card-title"> TOTAL STUDENTS AND LECTURERS</h5>
        	<ul class="list-group list-group-flush">
        		<li class="list-group-item"> Student 	: {{$students}}</li>
        		<li class="list-group-item"> Lecturers 	: {{$lecturer}}</li>
        	</ul>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection